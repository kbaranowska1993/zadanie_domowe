plansza = [0,1,2,3,4,5,6,7,8]
plansza2 = plansza[:]
ruch = 'O'
import random

def wyswietl_plansze(plansza):

    print("\n\t", plansza[0], "|", plansza[1], "|", plansza[2])
    print("\t", "---------")
    print("\t", plansza[3], "|", plansza[4], "|", plansza[5])
    print("\t", "---------")
    print("\t", plansza[6], "|", plansza[7], "|", plansza[8], "\n")


def winner(plansza):

    win = ((0, 1, 2),
                   (3, 4, 5),
                   (6, 7, 8),
                   (0, 3, 6),
                   (1, 4, 7),
                   (2, 5, 8),
                   (0, 4, 8),
                   (2, 4, 6))

    for row in win:
        if plansza[row[0]] == plansza[row[1]] == plansza[row[2]]:
            winner = plansza[row[0]]
            return winner

def gra():
    ruch = 'O'
    print('Zagrajmy w kółko i krzyżyk. Komputer zaczyna i jest kółkiem!')

    while not winner(plansza):
        if len(plansza2) == 0:
            print('Jest remis!')
            break
        elif ruch == 'O':
            computer = random.choice(plansza2)
            plansza[plansza.index(computer)] = 'O'
            del plansza2[plansza2.index(computer)]
            ruch = 'X'

        else:
            wyswietl_plansze(plansza)
            print('Twój ruch! Wybierz pole od 0-8, poza zajętymi')
            czlowiek = int(input('Podaj pole:'))
            # if (plansza.index(czlowiek)) == 'X' or (plansza.index(czlowiek)) == 'O':
            #     print('To pole jest już zajęte! Wybierz inne!')

            plansza[plansza.index(czlowiek)] = 'X'
            del plansza2[plansza2.index(czlowiek)]
            ruch = "O"

    if len(plansza2) != 0 and winner(plansza) == 'X':
        print('Brawo! WYGRAŁEŚ!')
    else:
        print('Wygrał komputer!')

gra()
wyswietl_plansze(plansza)
winner(plansza)
